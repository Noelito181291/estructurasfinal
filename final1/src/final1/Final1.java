/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final1;
import java.io.*;
/**
 *
 * @author Noel
 */
   
class Final1{
    public static void main(String args[])throws IOException{
        Pila pilaChar;
        char ch;
        boolean esPalindromo;
        String palabra;
        BufferedReader entrada=new BufferedReader(new InputStreamReader(System.in));
        try{
        pilaChar=new Pila();//crea pila vacia
        System.out.println("Teclea la palabra"+" a vereficar si es palindromo:");
        System.out.flush();
        palabra=entrada.readLine();
        //se crea la pila con los caracteres de la palabra
        pilaChar=new Pila();
        for(int i=0; i<palabra.length();)
        pilaChar.insertar(palabra.charAt(i++));
        //se comprueba si es palindromo
        esPalindromo=true;
        for(int j=0;esPalindromo&& !pilaChar.pilaVacia();)
        {
        esPalindromo=palabra.charAt(j++)==pilaChar.quitar();
        }
        pilaChar.limpiarPila();
        if(esPalindromo)
        System.out.println("La palabra "+palabra+" es un palindromo \n");
        else
        System.out.println("la palabra "+palabra+" no es un palindromo \n");
        }
        catch(Exception er)
        {
        System.out.println("Exception;"+er);
        }
    }
}   
