/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final1;
import java.io.*;
/**
 *
 * @author Noel
 */
class Pila{//creando la clase pila
        private static final int TAMPILA=79;
        //private char elemento;//se agrego este codigo para
        private int cima;
        private char []listaPila;
        //constructor de la clase pila
        public Pila()
        {
        cima=-1;
        listaPila=new char[TAMPILA];
        }
        public void insertar(char elemento) throws Exception
        {
        if(pilaLlena())
        {
        throw new Exception("Desbordamiento de pila");
        }
        cima++;
        listaPila[cima]=elemento;
        }
        public char quitar()throws Exception
        {
        char aux;
        if(pilaVacia())
        {
        throw new Exception("pila vacia, no se puede exceder");
        }
        aux=listaPila[cima];
        cima--;
        return aux;
        }
        public boolean pilaVacia()
        {
        return cima==-1;
        }
        public boolean pilaLlena()
        {
        return cima==TAMPILA-1;
        }
        public void limpiarPila()
        {
        cima=-1;
        }
}