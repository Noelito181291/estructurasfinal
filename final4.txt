Las estructuras de datos son una forma de organizar los datos en la computadora, de tal manera que 
nos permita realizar unas operaciones con ellas de forma muy eficiente es decir, igual que un array
introducimos un dato y eso es prácticamente inmediato, no siempre lo es, según qué estructuras de
datos y qué operaciones depende que algoritmo queramos ejecutar, habrá veces que sea mejor utilizar 
una estructura de datos

-->PILAS.- Una pila es una estructura que nos permite apilar elementos y recopilarlos en el orden inverso al 
cual los apilamos mediante operaciones de desapilar. Esto es lo que se conoce como estructuras 
LIFO (Last In First Out). De esta manera una pila suele tener 3 operaciones básicas:

*apilar, añade un elemento a la lista.
*desapilar, retira un elemento de la lista
*¿está vacía?, comprueba si la lista está vacía.

-->COLAS.- En Programación, se le llama “Cola” al Tipo de Dato Abstracto que es una 
lista en la que sus elementos se introducen (Encolan) únicamente por un extremo que le 
llamamos “Final de la Cola” y se remueven (Desencolan) únicamente por el extremo contrario al que le 
llamamos “Frente de la Cola” o “Principio de la Cola”.

-->LISTAS.- Una lista es una secuencia de elementos dispuesto en un cierto orden, en la que cada elemento 
tiene como mucho un predecesor y un sucesor. El número de elementos de la lista no suele estar fijado, 
ni suele estar limitado por anticipado. Representaremos la estructura de datos de forma gráfica con cajas
 y flechas. Las cajas son los elementos y las flechas simbolizan el orden de los elementos. › La estructura
 de datos deberá permitirnos determinar cuál es el primer elemento y el último de la estructura, cuál es su
 predecesor y su sucesor (si existen de cualquier elemento dado). Cada uno de los elementos de información 
suele denominarse nodo.

-->ARBOLES.-La estructura de datos árbol al contrario que las listas es una estructura de datos no lineal. 
Las listas tienen un orden impuesto en sus elementos teniendo como mucho un predecesor y un sucesor. 
Los árboles pueden tener dos o más sucesores.

Un árbol consiste en un conjunto de nodos o vértices y un conjunto de aristas o arcos que satisface 
unos requisitos:

*Existe una jerarquía de nodos, de forma que a cada nodo hijo le llega una arista de otro nodo padre. 
De esta forma se establece la relación padre-hijo: p es padre de h, h es un hijo de p.

*El nodo donde comienza la jerarquía se llama nodo raíz. A este nodo no llegan arcos de ningún otro nodo, 
en otras palabras, no es hijo de ningún nodo padre.

*Existe un camino único entre la raíz y cualquiera de los nodos del árbol