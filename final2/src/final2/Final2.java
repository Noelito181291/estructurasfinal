/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final2;
import java.util.Scanner;
/**
 *
 * @author Alvaro Noel Ventura CI 8655591
 */
public class Final2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        cola c=new cola();
        Scanner op = new Scanner(System.in);
        boolean salir = false;
        int opcion;
        while(!salir){ 
           System.out.println("1. ingresar datos");
           System.out.println("2. sumar datos");
           System.out.println("3. Salir");
            
           System.out.println("Escribe una de las opciones");
           opcion = op.nextInt();
           switch(opcion){
               case 1:
                   c.insertar();
                   break;
               case 2:
                   c.sumar();
                   break;
                case 3:
                   salir=true;
                   break;
                default:
                   System.out.println("Solo números entre 1 y 3");
           }
       }
    }
    
}
